const {Solar, Lunar, Foto, NineStar, EightChar, SolarWeek, SolarMonth, SolarSeason, SolarHalfYear, SolarYear, LunarMonth, LunarYear, LunarTime, ShouXingUtil, SolarUtil, LunarUtil, FotoUtil, HolidayUtil} = require('./lunar.js')

module.exports = {
  Solar: Solar,
  Lunar: Lunar,
  Foto: Foto,
  NineStar: NineStar,
  EightChar: EightChar,
  SolarWeek: SolarWeek,
  SolarMonth: SolarMonth,
  SolarSeason: SolarSeason,
  SolarHalfYear: SolarHalfYear,
  SolarYear: SolarYear,
  LunarMonth: LunarMonth,
  LunarYear: LunarYear,
  LunarTime: LunarTime,
  ShouXingUtil: ShouXingUtil,
  SolarUtil: SolarUtil,
  LunarUtil: LunarUtil,
  FotoUtil: FotoUtil,
  HolidayUtil: HolidayUtil
}
